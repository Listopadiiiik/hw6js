let btn = document.querySelector('.btn')

btn.addEventListener('click', async function getIp(){
    let response = await fetch('https://api.ipify.org/?format=json');
    let user = await response.json();

    let response2 = await fetch(`http://ip-api.com/json/${user.ip}`);
    let user2 = await response2.json();
    console.log(user2)

    btn.insertAdjacentHTML('afterend', `<p>IP: ${user.ip}</p><p>Country: ${user2.country}</p><p>Region code: ${user2.region}</p><p>City: ${user2.city}</p><p>District: ${user2.zip}</p><p>Region: ${user2.regionName}</p>`)
})